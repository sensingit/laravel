<?php
namespace App\Auth;

use Illuminate\Auth\TokenGuard as BaseTokenGuard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

/**
 * TODO: remove when https://github.com/laravel/internals/issues/401 is handled
 * Provides fix for TokenGuard having a static storage key so it works with doctrine
 *
 * Class TokenGuard
 * @package App\Auth
 */
class TokenGuard extends BaseTokenGuard
{
    /**
     * {@inheritDoc}
     */
    public function __construct(UserProvider $provider, Request $request)
    {
        parent::__construct($provider, $request);
        $this->storageKey = "apiToken";
    }
}