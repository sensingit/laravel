<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    /** @var \App\Models\User $identity */
    $identity = $request->user();
    return response()->json([
        "identity" => $identity->getArrayCopy(),
    ]);
});

Route::get('/test', function (Request $request) {
    /** @var \App\Models\User $identity */
    $identity = $request->user();
    return response()->json([
        "identity" => $identity === null ? $identity : $identity->getArrayCopy(),
    ]);
});
